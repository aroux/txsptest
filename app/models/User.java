package models;

import java.util.*;
import javax.persistence.*;

import play.data.validation.Constraints.*;
import play.db.ebean.*;

@Entity
public class User extends Model {

    @Id
    public Long id;
    @Required
    public String fullName;
    @Required
    public String pseudo;
    @Required
    @Email
    public String email;
    @Required
    public String password;

    //Queries
    public static Model.Finder<Long, User> find = new Model.Finder(Long.class, User.class);


}