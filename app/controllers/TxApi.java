package controllers;

import models.User;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.node.JsonNodeFactory;
import play.*;
import play.mvc.*;
import play.libs.Json;
import org.codehaus.jackson.node.ObjectNode;

import views.html.*;

public class TxApi extends Controller {

    public static Result login(){
        return ok();
    }

    public static Result getUserProfileByPseudo(String pseudo){
        User found = User.find.where().eq("pseudo", pseudo).findUnique();
        ObjectNode result = Json.newObject();

        if(found == null) {
            result.put("status", "KO");
            result.put("message", "No users found");
            return badRequest(result);
        } else {
            result.put("status", "OK");
            result.put("message", "Hello " + found.fullName);
            return ok(result);
        }
    }

}
