package controllers;

import models.*;
import play.*;
import play.data.Form;
import play.mvc.*;

import views.html.*;

import java.util.List;

import static play.data.Form.form;

public class Users extends Controller {

    final static Form<User> userForm = form(User.class);

    public static Result add(){
        Form<User> createUser = userForm.bindFromRequest();

        if(createUser.hasErrors()) {

            return badRequest(index.render("Index",createUser));
        } else {
            User user = createUser.get();
            user.save();

            return redirect("/users");
        }


    }

    public static Result list(){

        List<User> userList = User.find.all();
        return ok(list.render("List user",userList));

    }

}
